import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPage } from './login/pages/login.page';
import { PokemonsPage } from './pokemon/pokemons.page';
import { TrainerPage } from './trainer/pages/trainer.page';
import { AuthenticationGuard } from './authentication.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login',
  },
  {
    path: 'pokemons',
    component: PokemonsPage,
    canActivate: [AuthenticationGuard],
  },
  {
    path: 'login',
    component: LoginPage,
  },
  {
    path: 'trainer',
    component: TrainerPage,
    canActivate: [AuthenticationGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
