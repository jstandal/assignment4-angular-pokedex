import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  Routes,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthguardServiceService } from './authguard-service.service';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationGuard implements CanActivate {
  constructor(
    private Authguardservice: AuthguardServiceService,
    private router: Router
  ) {}
  //Authentication method to pass to router
  canActivate(): boolean {
    if (!this.Authguardservice.getToken()) {
      alert('Log in to view this page');
      this.router.navigateByUrl('/login');
    }
    return this.Authguardservice.getToken();
  }
}
