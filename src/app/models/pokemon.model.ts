export interface Pokemon {
  name: string;
  url: string;
  sprites: Object;
}

export interface FetchResult {
  count: number;
  next: string;
  previous: string;
  results: Pokemon[];
}
