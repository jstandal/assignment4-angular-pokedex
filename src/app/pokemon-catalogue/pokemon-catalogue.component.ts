import { Component, OnInit } from '@angular/core';
import { PokemonsCollectedService } from '../services/pokemons-collected.service';
import { PokemonsService } from '../services/pokemons.service';
import { LocalStorageUtil } from '../utils/localstorage.util';
import { StorageKeys } from '../utils/storage-keys.enum';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.component.html',
  styleUrls: ['./pokemon-catalogue.component.css'],
})
export class PokemonCatalogueComponent implements OnInit {
  constructor(
    private readonly pokemonService: PokemonsService,
    private readonly pokemonCollectedService: PokemonsCollectedService
  ) {}

  ngOnInit(): void {
    this.pokemonService.fetchPokemons();
  }

  get pokemons(): any[] {
    return this.pokemonService.pokemons();
  }

  onPokemonClicked(pokemon: any): void {
    this.pokemonCollectedService.setPokemonCollection(pokemon);
    this.pokemonService.setPokemonSelected(pokemon);
    this.pokemonCollectedService.setLocalCollection();
  }
}
