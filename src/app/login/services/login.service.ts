import { Injectable } from '@angular/core';
import { LocalStorageUtil } from 'src/app/utils/localstorage.util';
import { StorageKeys } from 'src/app/utils/storage-keys.enum';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  public login(trainerName: string): void {
    LocalStorageUtil.set(StorageKeys.Trainer, trainerName);
  }
}
