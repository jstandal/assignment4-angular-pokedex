import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { PokemonCatalogueComponent } from './pokemon-catalogue/pokemon-catalogue.component';
import { PokemonsCollectedComponent } from './pokemons-collected/pokemons-collected.component';
import { FormsModule } from '@angular/forms';
import { PokemonsPage } from './pokemon/pokemons.page';
import { AppRoutingModule } from './app-routing.module';
import { LoginFormComponent } from './login/components/login-form/login-form.component';
import { LoginPage } from './login/pages/login.page';
import { HeaderComponent } from './header/header.component';
import { TrainerPage } from './trainer/pages/trainer.page';
import { AuthguardServiceService } from './authguard-service.service';

@NgModule({
  declarations: [
    AppComponent,
    PokemonCatalogueComponent,
    PokemonsCollectedComponent,
    PokemonsPage,
    LoginFormComponent,
    LoginPage,
    HeaderComponent,
    TrainerPage,
  ],
  imports: [BrowserModule, HttpClientModule, FormsModule, AppRoutingModule],
  providers: [AuthguardServiceService],
  bootstrap: [AppComponent],
})
export class AppModule {}
