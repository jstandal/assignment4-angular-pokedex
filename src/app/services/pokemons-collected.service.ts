import { Injectable } from '@angular/core';
import { LocalStorageUtil } from '../utils/localstorage.util';
import { StorageKeys } from '../utils/storage-keys.enum';

@Injectable({
  providedIn: 'root',
})
export class PokemonsCollectedService {
  private _pokemonSelection: any[] = [];

  //Adds pokemon to collection or removes it if allready in collection.
  public setPokemonCollection(pokemon: any): void {
    const index = this._pokemonSelection.indexOf(pokemon);
    if (index === -1) {
      this._pokemonSelection = [...this._pokemonSelection, pokemon];
    } else {
      this._pokemonSelection.splice(index, 1);
    }
  }

  //Adds collection to local storage
  public setLocalCollection(): void {
    LocalStorageUtil.set(StorageKeys.TrainerCollection, this._pokemonSelection);
  }

  public pokemons(): any {
    return this._pokemonSelection;
  }
}
