import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';

@Injectable({
  providedIn: 'root',
})
export class PokemonsService {
  private _pokemons: any[] = [];
  private _error: string = '';

  constructor(private readonly http: HttpClient) {}

  //Nested fetch to get info for each pokemon after getting pokemonlist
  public fetchPokemons(): void {
    this._pokemons = [];
    this.http.get('https://pokeapi.co/api/v2/pokemon?limit=151').subscribe(
      (response: any) => {
        response.results.forEach((result: { name: string }) => {
          this.fetchPokemonData(result.name).subscribe((pokemon: any) => {
            //Adds a collected property to the pokemon object
            this._pokemons.push({ ...pokemon, collected: false });
          });
        });
      },
      (error: HttpErrorResponse) => {
        this._error = error.message;
      }
    );
  }

  public fetchPokemonData(name: string) {
    return this.http.get(`https://pokeapi.co/api/v2/pokemon/${name}`);
  }

  //Changes the collected property of a given pokemon
  public setPokemonSelected(pokemon: any): void {
    const index = this._pokemons.indexOf(pokemon);
    console.log(index);

    if (index !== -1) {
      const booleanValue = this._pokemons[index].collected ? false : true;
      this._pokemons[index].collected = booleanValue;
      console.log((this._pokemons[index].collected = booleanValue));
    }
  }

  //Getter method with sorting function to return the pokemons in the right order
  public pokemons(): Pokemon[] {
    return this._pokemons.sort((a, b) =>
      a.id > b.id ? 1 : b.id > a.id ? -1 : 0
    );
  }

  public error(): string {
    return this._error;
  }
}
