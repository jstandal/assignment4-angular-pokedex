import { Injectable } from '@angular/core';
import { StorageKeys } from './utils/storage-keys.enum';

@Injectable({
  providedIn: 'root',
})
export class AuthguardServiceService {
  constructor() {}
  getToken() {
    return !!localStorage.getItem(StorageKeys.Trainer);
  }
}
