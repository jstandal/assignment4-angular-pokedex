import { Component } from '@angular/core';
import { PokemonsCollectedService } from '../services/pokemons-collected.service';
import { LocalStorageUtil } from '../utils/localstorage.util';
import { StorageKeys } from '../utils/storage-keys.enum';

@Component({
  selector: 'app-pokemons-collected',
  templateUrl: './pokemons-collected.component.html',
  styleUrls: ['./pokemons-collected.component.css'],
})
export class PokemonsCollectedComponent {
  get pokemons(): any[] | null {
    const localStorage: [] | null = LocalStorageUtil.get(
      StorageKeys.TrainerCollection
    );
    if (localStorage) {
      return localStorage;
    } else {
      return null;
    }
  }
}
