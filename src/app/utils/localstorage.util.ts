export class LocalStorageUtil {
  public static set<T>(key: string, value: T): void {
    try {
      const json: string = JSON.stringify(value);
      localStorage.setItem(key, json);
    } catch (error) {
      throw new Error(error.message);
    }
  }

  public static get<T>(key: string): T | null {
    const response: string | null = localStorage.getItem(key);
    if (!response) {
      return null;
    }

    try {
      return JSON.parse(response) as T;
    } catch (error) {
      return null;
    }
  }

  public static delete(key: string): void {
    localStorage.removeItem(key);
  }

  public static clear(): void {
    localStorage.clear();
  }
}
